﻿using System;

class FloydWarshallAlgorithm
{
    // Константа для представлення нескінченності відстані між вершинами
    static int INF = int.MaxValue;

    // Функція для виведення матриці відстаней
    static void PrintSolution(int[,] dist, int V)
    {
        Console.WriteLine("Матриця відстаней між вершинами:");
        for (int i = 0; i < V; ++i)
        {
            for (int j = 0; j < V; ++j)
            {
                // Якщо відстань є нескінченною, виводимо "INF"
                if (dist[i, j] == INF)
                    Console.Write("INF ");
                else
                    Console.Write(dist[i, j] + "   ");
            }
            Console.WriteLine();
        }
    }

    // Алгоритм Флойда-Уоршелла для знаходження найкоротших шляхів між усіма пара-ми вершин
    static void FloydWarshall(int[,] graph, int V)
    {
        // dist[][] буде матрицею відстаней між вершинами
        int[,] dist = new int[V, V];







        // Ініціалізація матриці відстаней так, що dist[i, j] буде відстанню від вершини i до j
        for (int i = 0; i < V; ++i)
            for (int j = 0; j < V; ++j)
                dist[i, j] = graph[i, j];

        // Проходимо по усім вершинам і розглядаємо їх як можливі проміжні вершини
        for (int k = 0; k < V; ++k)
        {
            // Вибираємо вершину k як проміжну і переглядаємо всі пари вершин (i, j)
            for (int i = 0; i < V; ++i)
            {
                for (int j = 0; j < V; ++j)
                {
                    // Якщо вершина k дозволяє зменшити відстань між вершинами (i, j), оновлюємо значення
                    if (dist[i, k] != INF && dist[k, j] != INF && dist[i, k] + dist[k, j] < dist[i, j])
                        dist[i, j] = dist[i, k] + dist[k, j];
                }
            }
        }

        // Виводимо матрицю відстаней
        PrintSolution(dist, V);
    }

    public static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode; //Підключення  юні-коду для латинських літер
        Console.InputEncoding = System.Text.Encoding.Unicode;

        // Приклад графу. Матриця відстаней між вершинами
        int[,] graph = { {0, 5, INF, 10},
                         {INF, 0, 3, INF},
                         {INF, INF, 0, 1},
                         {INF, INF, INF, 0} };

        int V = 4; // Кількість вершин у графі
        FloydWarshall(graph, V); // Виклик алгоритму Флойда-Уоршелла
        Console.ReadLine();
    }
}
